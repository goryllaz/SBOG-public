package cz.janurbanec.sbogbackend.exception;

public class UserExistException extends RuntimeException {
    public UserExistException(String username) {
        super("Username " + username + " already exist");
    }
}
