package cz.janurbanec.sbogbackend.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity(name = "COMMENTS")
public class Comment {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    @Column(name = "ID_COMMENT", columnDefinition = "VARCHAR2(40)")
    private UUID id;

    @Basic
    @Column(name = "COMMENT_TEXT", columnDefinition = "VARCHAR2(4000)")
    private String text;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE", columnDefinition = "DATE")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date date;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_USER")
    @JsonIgnore
    private User user;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_EXTRA_COMMIT")
    @JsonIgnore
    private ExtraCommit extraCommit;

    public Comment() {
    }

    public Comment(String text, Date date) {
        this.text = text;
        this.date = date;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ExtraCommit getExtraCommit() {
        return extraCommit;
    }

    public void setExtraCommit(ExtraCommit extraCommit) {
        this.extraCommit = extraCommit;
    }
}
