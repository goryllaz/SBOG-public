package cz.janurbanec.sbogbackend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Table(name = "PROJECTS")
@Entity(name = "PROJECTS")
public class Project {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    @Column(name = "ID_PROJECT", columnDefinition = "VARCHAR2(40)")
    private UUID id;

    @Column(name = "NAME_PROJECT", columnDefinition = "VARCHAR2(260)")
    private String name;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATE_DATE", columnDefinition = "DATE")
    @DateTimeFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_USER")
    @JsonIgnore
    private User user;

    @OneToMany(mappedBy = "project")
    @JsonIgnore
    private List<ExtraCommit> extraCommits;

    public Project() {
    }

    public Project(String name, Date date, User user) {
        this.name = name;
        this.date = date;
        this.user = user;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<ExtraCommit> getExtraCommits() {
        return extraCommits;
    }

    public void setExtraCommits(List<ExtraCommit> extraCommits) {
        this.extraCommits = extraCommits;
    }

    public List<Commit> getCommits(String branchName) {
        List<Commit> commits = new ArrayList<>();
        try {
            Git git = new Git(getRepository());
            if (git.branchList().call().stream().anyMatch(ref ->
                    ref.getName().substring(
                            ref.getName().lastIndexOf("/") + 1
                    ).equals(branchName)
            ))
                git.log().add(getRepository().resolve(branchName)).call().forEach(revCommit ->
                        {
                            ExtraCommit tmpCommit = new ExtraCommit();
                            tmpCommit.setType(CommitType.NO_STATUS);
                            commits.add(
                                    new Commit(
                                            revCommit.getName(),
                                            revCommit.getAuthorIdent().getName(),
                                            revCommit.getAuthorIdent().getEmailAddress(),
                                            revCommit.getAuthorIdent().getWhen(),
                                            revCommit.getFullMessage(),
                                            getExtraCommits().stream().findFirst().filter(extraCommit ->
                                                    extraCommit.getName().equals(revCommit.getName())
                                            ).orElse(tmpCommit).getType()
                                    )
                            );
                        }
                );
        } catch (IOException | GitAPIException e) {
            e.printStackTrace();
        }

        return commits;
    }

    public String[] getBranches() {
        String[] branches = new String[0];
        try {
            Git git = new Git(getRepository());
            final String[] finalBranches = new String[countBranches()];
            final AtomicInteger i = new AtomicInteger(0);
            git.branchList().call().forEach(ref ->
                    finalBranches[i.getAndIncrement()] = ref.getName().substring(
                            ref.getName().lastIndexOf("/") + 1
                    )
            );
            branches = finalBranches;
        } catch (IOException | GitAPIException e) {
            e.printStackTrace();
        }

        return  branches;
    }

    private int countBranches() throws IOException, GitAPIException {
        Git git = new Git(getRepository());

        return git.branchList().call().size();
    }

    private FileRepository getRepository() throws IOException {
        return new FileRepository(
                System.getProperty("user.home")+"/SBOG/Users/"+user.getUsername()+"/"+getName()+"/.cz.janurbanec.sbogbackend.git"
        );
    }
}
