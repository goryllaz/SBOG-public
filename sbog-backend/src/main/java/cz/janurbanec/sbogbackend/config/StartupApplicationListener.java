package cz.janurbanec.sbogbackend.config;

import cz.janurbanec.sbogbackend.SbogBackendApplication;
import cz.janurbanec.sbogbackend.git.RepositoryManager;
import cz.janurbanec.sbogbackend.model.Project;
import cz.janurbanec.sbogbackend.model.Role;
import cz.janurbanec.sbogbackend.model.User;
import cz.janurbanec.sbogbackend.repository.ProjectRepository;
import cz.janurbanec.sbogbackend.repository.UserRepository;
import cz.janurbanec.sbogbackend.service.AddUserService;
import java.io.IOException;
import java.util.Date;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private AddUserService addUserService;

  @Autowired
  private ProjectRepository projectRepository;

  private static final Logger LOG = Logger.getLogger(SbogBackendApplication.class);

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    if (userRepository.findByUsername("sbog") == null) {
      LOG.info("Creating initial administrator");
      User admin = new User("sbog", System.getenv("SBOG_EMAIL"), System.getenv("SBOG_PWD"), Role.ADMIN);
      try {
        addUserService.saveUser(admin);
      } catch (IOException | InterruptedException e) {
        e.printStackTrace();
      }
      LOG.info("SBOG administrator is created");
    } else {
      LOG.info("SBOG administrator has already been created");
    }

    LOG.info("Checking gitolite admin repository");
    final User admin = userRepository.findByUsername("sbog");
    RepositoryManager.getRepositoriesByMember(admin.getUsername()).forEach(repository -> {
      if (!projectRepository.existsByNameAndUser(repository, admin)){
        LOG.info("Add repository " + repository + " to database");
        projectRepository.save(new Project(repository, new Date(), admin));
      }
    });
  }
}
