package cz.janurbanec.sbogbackend.git;

import java.io.File;
import java.io.IOException;
import nl.tudelft.ewi.gitolite.git.GitException;
import nl.tudelft.ewi.gitolite.git.NativeGitManager;

public class SbogGitManager extends NativeGitManager {

  public SbogGitManager(File workingDirectory) {
    super(workingDirectory);
  }

  @Override
  public void commitChanges() throws IOException, GitException, InterruptedException {
    ProcessBuilder processBuilder = new ProcessBuilder("git", "add", "*").directory(workingDirectory);
    Process process = processBuilder.start();

    process.waitFor();
    int exitValue = process.exitValue();

    if(exitValue != 0) {
      throw new GitException();
    }
    super.commitChanges();
  }
}
