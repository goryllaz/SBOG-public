package cz.janurbanec.sbogbackend.controller;

import cz.janurbanec.sbogbackend.model.Commit;
import cz.janurbanec.sbogbackend.model.Project;
import cz.janurbanec.sbogbackend.model.User;
import cz.janurbanec.sbogbackend.repository.ProjectRepository;
import cz.janurbanec.sbogbackend.repository.UserRepository;
import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/api/project/add")
    public ResponseEntity<Project> createProject(@RequestBody Project project) {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (!projectRepository.existsByNameAndUser(project.getName(), user)) {
            projectRepository.save(project);

            File projectFolder = new File(
                    System.getProperty("user.home")+"/SBOG/Users/" +
                            user.getUsername() +
                            "/" +
                            project.getName()
            );
            projectFolder.mkdir();
            try {
                Git git = Git.init().setDirectory(projectFolder).call();
            } catch (GitAPIException e) {
                e.printStackTrace();
            }

            return new ResponseEntity<>(
                    project,
                    HttpStatus.CREATED
            );
        }

        return new ResponseEntity<>(
            project,
            HttpStatus.CONFLICT
        );
    }

    @GetMapping("/api/project/getProject")
    public HttpEntity<Project> getProject(@RequestParam(name = "projectId") UUID projectId) {
        Project project = projectRepository.findById(projectId).get();

        return new ResponseEntity<>(
                project,
                HttpStatus.OK
        );
    }

    @DeleteMapping("/api/project/delete")
    public ResponseEntity<String> createProject(@RequestParam(name = "projectId") UUID projectId) {
        User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        if (projectRepository.existsById(projectId)) {
            Project project = projectRepository.getById(projectId);

            File projectFolder = new File("Users/"+user.getUsername()+"/"+project.getName());
            try {
                FileUtils.deleteDirectory(projectFolder);
            } catch (IOException e) {
                e.printStackTrace();
            }

            projectRepository.delete(project);
            return new ResponseEntity<>(
                    "deleted",
                    HttpStatus.OK
            );
        }

        return new ResponseEntity<>("Not found", HttpStatus.NOT_FOUND);
    }

    @GetMapping("/api/project/getCommits")
    public ResponseEntity<List<Commit>> getCommits(
            @RequestParam(name = "projectId") UUID projectId,
            @RequestParam(name = "branchName") String branchName
    ) {
        if (projectRepository.existsById(projectId)) {
            Project project = projectRepository.getById(projectId);

            return new ResponseEntity<>(
                    project.getCommits(branchName),
                    HttpStatus.OK
            );
        }

        return new ResponseEntity<>(
                null,
                HttpStatus.NOT_FOUND
        );
    }

    @GetMapping("/api/project/getBranches")
    public ResponseEntity<String[]> getBranches(@RequestParam(name = "projectId") UUID projectId) {
        if (projectRepository.existsById(projectId)) {
            Project project = projectRepository.getById(projectId);

            return new ResponseEntity<>(
                    project.getBranches(),
                    HttpStatus.OK
            );
        }

        return new ResponseEntity<>(
                new String[0],
                HttpStatus.NOT_FOUND
        );
    }
}

