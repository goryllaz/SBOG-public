package cz.janurbanec.sbogbackend.service;

public interface LoginService {
    String findLoggedInUsername();

    boolean isLoggedIn();
}
